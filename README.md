# Ejercicios

Ejercicios de la materia

GUIA PRÁCTICA
Ejercicios en programación
CARRERA: Licenciatura en Gestión de
Tecnologías de la Información
ASIGNATURAS:
 Introducción a la Programación
 Algoritmos y Programación
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
Estructuras básicas de programación
1. Hacer un programa que ingresas un número entero y por pantalla mostrar el doble de ese
número.
2. Hacer un programa que ingresas la edad de una persona y calculas cuantos años va a
tener dentro de 23 años. Mostrar el resultado por pantalla.
3. Hacer un programa que ingresas un precio y calculas el iva (21%). Mostrar el resultado por
pantalla
4. Hacer un programa que ingresas un precio y calculas un descuento del 9%. Mostrar por
pantalla el resultado
5. Hacer un programa que ingresas un número que representa cierta cantidad de una fruta y
dividís esa cantidad por 3 chicos. Mostrar por pantalla el resultado
6. Hacer un algoritmo que nos permita introducir un número, luego muestre por pantalla el
mensaje “Este número es positivo” o “Este número es negativo”
7. Hacer un algoritmo que nos permita introducir un número, luego muestre por pantalla el
mensaje “Este número es par” o “este número es impar”
8. Hacer un algoritmo que pida un número entero y determine si es múltiplo de 2. Muestra
un mensaje por pantalla
9. Hacer un algoritmo que pida un número entero y determine si es múltiplo de 5. Muestra
un mensaje por pantalla.
10. Hacer un algoritmo que ingrese dos números y determinar si alguno de los dos números
es divisible por 3.
11. Hacer un algoritmo que ingresa una letra, luego mostrarla por pantalla.
12. Hacer un algoritmo que ingresa 2 letras, mostrarlas por pantalla ordenadas
alfabéticamente.
13. Hacer un algoritmo que ingrese un número decimal, súmale 15 y dividí por 2 mostrar el
resultado por pantalla en formato decimal
14. Un señor tiene 10 pares de zapatillas, esa cantidad la ingresa a su computadora, en el
momento del ingreso le llegan 5 pares más, que debe ingresar también en la
computadora. Mostrar por pantalla cuantas zapatillas tiene en total
15. Hacer un algoritmo que ingresa un número, mostrar por pantalla si es par
16. Hacer un algoritmo que se ingresa un precio( número decimal) y le calcula un
incremento de 10% Mostrar por pantalla el resultado
17. Hacer un algoritmo que se ingresa dos precios(numero decimal) Mostrar los precios
ordenado de mayor a menor
18. Hacer un algoritmo que se ingresa el tiempo registrado de dos autos que corrieron una
carrera. El tiempo se guardó como número decimal. Mostrar por pantalla cual llego
primero
19. Hacer un algoritmo que se ingresa tres número. Mostrar por pantalla si están ordenados
de menor a mayor
20. Hacer un algoritmo que permita ingresar tres números enteros y, si el primero de ellos es
negativo, calcular el producto de los tres, en caso contrario calcular la suma de ellos.
21. Diseñar un algoritmo que calcule la superficie de un triángulo a partir del ingreso de su
base y altura y muestre por pantalla el resultado, con el mensaje “La superficie del
triángulo es:….”.
22. Realizar un algoritmo que permita leer dos números, determinar cuál de los dos números
es el menor y mostrarlo por pantalla con el mensaje “El numero N es menor al numero
X”
23. Diseñar un algoritmo que calcule la longitud de la circunferencia y el área del círculo.
Considerar que por pantalla se ingresa el radio de dicha circunferencia.
24. Realizar un algoritmo que permita determinar la hipotenusa de un triángulo rectángulo.
Si se considera como dato las longitudes de sus dos catetos y esto se ingresa por pantalla.
25. Realizar un algoritmo que pida un número y saque por pantalla su tabla de multiplicar.
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
Bucles
26. Hacer un algoritmo que se ingresan 100 números enteros, mostrar por pantalla la
cantidad de números pares que se ingresaron.
27. Hacer un algoritmo que se ingresan 50 números enteros, calcular cuántos números
impares se ingresaron
28. Hacer un algoritmo que se ingresan 20 números enteros, calcular cuántos números
impares y cuántos números pares fueron ingresados
29. Hacer un algoritmo donde se Ingresan 10 números enteros. Mostrar por pantalla el
número más grande ingresado y en qué posición se ingresó.
30. Hacer un algoritmo que se Ingresan las notas de 20 alumnos. Mostrar por pantalla la
mejor nota.
31. Hacer un algoritmo que se ingresa 10 letras. Mostrar cual es la mayor letra ingresada.
32. Hacer un algoritmo donde se Ingresan los siguientes datos de 20 alumnos:
nota, (entero),
sexo (char =F/M).
Mostrar por pantalla la mejor nota y si es alumno o alumna.
33. Hacer un algoritmo que registra la temperatura (número decimal), a las 15 hs. Durante
todo un mes cualquiera. Mostrar por pantalla la temperatura más alta de ese mes y la
más baja.
34. Hacer un algoritmo que registra la temperatura (número decimal), a las 15 hs. Por un
mes. Indicar el día que tuvo la menor temperatura y el valor de la misma.
35. Hacer un algoritmo que registre la lluvia de todo un mes. Indicar el día de mayor lluvia y
la cantidad de milímetro caídos.
36. Hacer un algoritmo que permita ingresar diez (10) números, luego muestre por pantalla
cuantos eran mayores a cero y cuantos son menores a cero.
37. Hacer un algoritmo que permita ingresar por pantalla cinco (5) números y luego calcular
su media
38. Hacer un algoritmo que pida la nota de un examen (un nº real entre 0 y 10) muestre por
pantalla la calificación de la siguiente forma:
a. Si la nota es menor que 5 la leyenda “En suspenso”
b. Si la nota se encuentra entre 5 inclusive y 7 sin incluir la leyenda “Aprobado”
c. Si la nota se encuentra entre 7 inclusive y 9 sin incluir la leyenda “Notable”
d. Si la nota se encuentra entre 9 inclusive y 10 sin incluir la leyenda
“Sobresaliente”
e. Si la nota es un 10 la leyenda “Matrícula de honor”.
Terminar el algoritmo cuando se ingresa cero como nota.
39. Hacer un algoritmo que permita ingresar por pantalla números naturales al finalizar
informar:
i. ¿Cuántos están entre el 50 y 75, ambos inclusive?
ii. ¿Cuántos mayores de 80?
iii. ¿Cuántos menores de 30?
El algoritmo debe finalizar cuando se ingresa el número 0.
40. Hacer un algoritmo que permita convertir calificaciones numéricas a letras, según la
siguiente tabla:
A=19 y 20, B=16, 17 y 18, C=13, 14 y 15, D=10, 11 y 12, E=1 hasta el 9. Se asume
que la nota está comprendida entre 1 y 20.
41. Hacer un algoritmo que permita ingresar un número entero. Si este no es negativo
calcular su factorial. (Realizar un bucle en el factorial), si es negativo pedir otro número.
El programa finaliza cuando se ingresa el número cero
42. Hacer un algoritmo que permita ingresar un número natural luego por pantalla informar
todos sus divisores.
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
44. Realizar un algoritmo para calcular el cuadrado de un número. El número debe ser mayor
que cero. Si es menor que cero mostrar por pantalla el mensaje "ERROR, el Número
debe ser mayor que cero". Si no mostrar el resultado de dicho calculo. El programa
finaliza cuando se ingresa cero
45. Diseñar un algoritmo que permita ingresar por pantalla 70 números naturales. Calcular el
promedio de los números pares. Luego mostrar por pantalla el resultado
46. Hacer un algoritmo para calcular el promedio de una lista de números positivos el
algoritmo finaliza cuando se ingresa el número cero.
47. Desarrollar un algoritmo que lea una lista de 20 números y determine cuantos son
positivos, y cuantos son negativos.
48. Escribir un programa que dados dos números, uno es un número real (base) y el otro un
numero entero positivo (exponente), sacar por pantalla la potencias con base al número
dado y exponentes el otro número introducido
49. Desarrollar un algoritmo que permita ingresar 50 números naturales, luego informar por
pantalla:
a) La sumatoria de los valores múltiplos de 3.
b) La cantidad de valores múltiplos de 5.
c) La sumatoria de los valores que se ingresan y son par.
50. Desarrollar un programa que permita ingresar los extremos de un intervalo cerrado, luego
ingresar 80 valores e informar cuantos pertenecen al intervalo ingresado anteriormente.
51. Desarrollar un programa que permita ingresar números reales mayores que 0 (cero),
finalizar el ingreso cuando no se cumpla esta condición e informar el valor menor y el
valor mayor del conjunto.
52. Un número natural es primo si el factorial del número introducido más uno ((N-1)!+1) es
divisible por N, utilizando esta propiedad realizar un programa que lea un número e
informe si es primo o no.
53. Realizar un programa que calcule e informe los 20 primeros números primos.
54. Como resultado de una encuesta se recogen los siguientes datos: sexo, edad y altura. Se
pide realizar un programa que informe el porcentaje de mujeres mayores de 25 años, la
cantidad de varones menores de 18 años y el porcentaje de individuos mayores de 18
años cuya altura supera los 170 cm. Ingresar únicamente 40 encuestas.
55. Hacer un algoritmo que realice la sumatoria de los números enteros comprendidos entre
el 1 y el 10, lo que se pide es: 1+2+3+….+10.
56. Hacer un algoritmo que realice la sumatoria de los números enteros múltiplos de 5,
comprendidos entre el 1 y el 100, lo que se pide es, 5 + 10 + 15 +…. + 100. El programa
deberá mostrar por pantalla los números en cuestión con un mensaje y finalmente su
sumatoria
57. Desarrollar un algoritmo que realice la sumatoria de los números enteros pares
comprendidos entre el 1 y el 100, es decir, 2+4+6+….+100. El programa deberá mostrar
por pantalla los números en cuestión con un mensaje y finalmente su sumatoria
58. Diseñar un algoritmo que permita ingresar 10 números, ninguno de ellos igual a cero. Se
pide sumar los positivos, obtener el producto de los negativos y luego mostrar ambos
resultados.
59. Desarrollar un algoritmo que ingrese por pantalla 300 números enteros y determine
cuántos de ellos son impares; al final deberá indicar su sumatoria.
60. Crear un programa que calcule la caída de potencial producida por una resistencia según
la ley de Ohm (V = I * R) a partir de la resistencia y la intensidad que pasa.
Nota: El programa no debe aceptar resistencias negativas, dado que no tienen sentido
físico, ni resistencias mayores que 1000Ω (requerimiento adicional del problema). En
ambos casos el programa debe escribir un mensaje de error en pantalla diciendo que el
valor de la resistencia está fuera de límites aceptables indicando la causa por la que
dicho valor, para la resistencia, ha sido rechazado.
61. Escribir un programa que pida un año y diga si es bisiesto o no.
Nota: Un año es bisiesto si es múltiplo de 4 salvo el caso en que sea múltiplo de 100, que
no es bisiesto, y no sea múltiplo de 400. Por ejemplo, el año 1900 no fue bisiesto, el 2000
sí y el 2100 no lo es.
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
62. Escribir un programa que ayude a aprender las tablas de multiplicar. Para ello se irá
pidiendo la tabla de multiplicar de un número (pedido por teclado con anterioridad) y se
comprobara que los valores introducidos sean correctos. Si es así el programa escribirá
“correcto” y en caso contrario deberá escribir “Lo siento, se ha equivocado. La respuesta
correcta era” La última línea mostrará el número de aciertos.
A continuación se muestra un ejemplo de ejecución:
Programa para aprender las tablas de multiplicar
Por pantalla se escribe la siguiente pregunta “¿Con qué número quieres practicar?” el
usuario ingresa el numero de la tabla que quiere practicar 5
Luego mostrar por pantalla 5 * 1 =
Dato introducido por el usuario 5
El programa envía el mensaje “Valor correcto”
Luego mostramos por pantalla 5 * 2 =
El usuario introduce 11
El programa envía el mensaje “Lo siento se ha equivocado. La respuesta correcta era 10”
Cuando finalizamos le enviamos el siguiente mensaje “Has acertado 9 veces de 10”
63. Hacer un programa que permita ingresar distintos caracteres el programa finaliza cuando
han ingresado 10 veces la letra 'a'. Por cada carácter ingresado y que no sea una 'a'
deberá mostrar un mensaje “No es la letra A”. Cuando ingresaron 10 letras 'a' el
programa terminará.
64. Hacer un programa que lea caracteres desde teclado y vaya contando las vocales que
aparecen. El programa terminará cuando lea el carácter # y entonces mostrará un
mensaje indicando cuántas vocales ha leído (cuántas de cada una de ellas).
65. Escribir un programa que primero pida por pantalla con cuántos números se va a trabajar
digamos que sean X) y luego pida los X números por pantalla. Después de introducir los X
números se mostrará un mensaje por pantalla indicando cuál es el mayor y menor valor
introducido, así como el promedio de todos los números introducidos.
66. A un trabajador le pagan según sus horas trabajadas y la tarifa está a un valor por hora. Si
la cantidad de horas trabajadas es mayor a 40 horas, la tarifa por hora se incrementa en
un 50% por considerarse que son horas extras. Desarrollar un programa que calcule el
salario del trabajador dadas las horas trabajadas y la tarifa
67. A un trabajador le descuentan de su sueldo el 10% si su sueldo es menor o igual a 1000,
por encima de 1000 y hasta 2000 el 5% del adicional, y por encima de 2000 el 3% del
adicional. Desarrollar un programa calcule el descuento y sueldo neto que recibe el
trabajador dado su sueldo.
68. Escribir el algoritmo que me permita leer un número decimal que representa una
cantidad de grados Celsius y convierta dicho valor a la cantidad equivalente en grados
Fahrenheit.
La salida del programa puede ser de la siguiente forma: 100 grados Celsius son 212 grados
Fahrenheit
69. Diseñar el algoritmo que permita ingresar dos valores por ejemplo NUM1 y NUM2 y luego
se intercambien los valores de dichas variables, es decir que el valor que tenía NUM1
ahora lo contenga NUM2 y viceversa
70. Escribir un programa que visualice una tabla de los N primeros números, siendo N un
Número que ingresa el usuario. Utilizar el siguiente diseño de salida suponiendo que el
Usuario ingresó un tres:
NÚMERO CUADRADO CUBO
1 1 1
2 4 8
3 9 27
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
71. Diseñar un algoritmo que permita ingresar 20 empleados de una fábrica su peso. Luego
mostrar por pantalla cuantos pesan menos 80 kg. Y cuantos pesan más de 80 kg.
72. En una tienda de artículos para caballeros al final del día se carga en la computadora las
boletas que confeccionaron los distintos vendedores para saber cuanto fue la comisión
del día de cada uno de ellos. Los datos que se ingresan (por boleta) son: el número de
Vendedor y el importe. Cuando no hay más boletas para cargar se ingresa 0. Teniendo en
Cuenta que el negocio tiene 3 vendedores y que el porcentaje sobre las ventas es del 5%,
Indicar cuánto ganó cada vendedor en el día.
73. Ingresar por teclado 3 números correspondientes a los lados de un triángulo. Teniendo en
cuenta que la suma de los dos lados menores tiene que ser superior al lado mayor para
que formen un triángulo, indicar si los números indicados forman un triángulo y si lo
forman que tipo de triángulo es (según sus lados).
74. Realizar un programa que solicite el valor hora de un empleado. Posteriormente se
ingresa el nombre del empleado, la antigüedad y la cantidad de horas trabajadas en el
mes. El programa debe calcular el importe a cobrar de la siguiente forma:
-El subtotal resulta de multiplicar el valor hora por la cantidad de horas trabajadas
-Al subtotal hay que sumarle la cantidad de años trabajados multiplicados por $30
-A este resultado hay que restarle el 13% en concepto de descuentos. Mostrar por
pantalla el recibo del empleado con el nombre, la antigüedad, el valor hora, el total a
cobrar en bruto, el total de descuentos y el valor neto a cobrar.
75. Se registra en un programa los siguientes datos de los empleados de una empresa Número
de legajo, sueldo y sexo (1-femenino y 2-masculino). Diseñar un algoritmo que permita
informar cuantas mujeres ganan más de $500 y cuantos hombres ganan menos de $400.
Finaliza el ingreso con el número cero en legajo
76. De los alumnos de una escuela se registra su apellido, nombre y su altura. Diseñar un
algoritmo que indique el nombre del alumno más alto y su altura (sólo uno es el más
alto).
77. Diseñar un algoritmo que permita calcular el promedio de un alumno sabiendo que los
datos que se ingresan por alumno es:
Nombre del alumno
Nombre de la materia
Seis notas de esa materia,
Cuando en nota se ingresa cero se finaliza el programa. Se pide mostrar: NOMBRE DEL
ALUMNO:……..PROMEDIO:……..
78. Del registro de partes meteorológico por cada día se registra la fecha, temperatura
máxima y temperatura mínima. Diseñar un algoritmo que permita informar:
a. El día más frío y cual fue esa temperatura
b. El día más cálido y cual fue esa temperatura
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
80. De las 20 participantes de un concurso de belleza se van registrando de uno en uno los
siguientes datos:
Apellido y Nombre
Puntos por inteligencia
Puntos por Cultura general
Puntos por Belleza
Se necesita informar por pantalla:
Apellido y nombre de la concursante de mayor puntaje general
Puntaje acumulado por todas las participantes en Cultura general, en Inteligencia y en
Belleza
C) De los tres puntajes acumulados en el ítem anterior cual fue el menor
81. Un profesor de matemática de un establecimiento educativo registra de cada alumno Nº
de legajo, nombre y promedio. Según el promedio desea saber cuántos alumnos
aprobaron (promedio mayor o igual a 7), cuantos rinden en diciembre (promedio menor a
7 y mayor o igual a 4) y cuantos rinden examen en marzo (promedio menor a 4). Además
desea conocer el Nº de legajo y nombre del alumno con mejor promedio.
82. En un club se registran de uno en uno los siguientes datos de sus socios:
Número de socio
Apellido y nombre
Edad
Tipo de deporte que practica (1 tenis, 2 rugby, 3 vóley, 4 hockey, 5 fútbol)
Diseñar un algoritmo que permita mostrar por pantalla el nombre y apellido de los socios
que practican tenis, de los que practican fútbol mostrar el promedio de edad, la cantidad
de los que practican rugby y vóley Del programa se sale colocando cero en tipo de
deporte
83. Realizar un programa en el cual si se ingresa una vocal se emite una leyenda indicando lo
sucedido y si se ingresa otro carácter se indica con una leyenda que diga “no se ha
ingresado una vocal”. Resuelva este problema utilizando switch-case
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
Vectores
85. Realizar un programa que cargue un vector de 15 posiciones.
86. Desarrollar un algoritmo que permita la carga de un vector de 10 posiciones. Generar una
rutina que transcriba el contenido del vector a otro vector en orden inverso.
a. Ejemplo dado: Vector Origen
1 1 2 2 3 4 6 7 9 5
Resultado Esperado: Vector Destino
5 9 7 6 4 3 2 2 1 1
87. Diseñe un programa que lea un vector de 10 posiciones, luego determine la primera
posición que contenga un número múltiplo de 3. En caso que no lo contenga deberá
informar por pantalla “No existe”
Ejemplo
1 1 2 2 6 4 6 7 9 5
Resultado Esperado: “El numero múltiplo de tres es 6 y está en la posición 4 “
88. Crear un programa que lea e imprima el promedio del total de los elementos pares de un
vector de 10 posiciones
89. Crear un programa con un vector de 5 posiciones que lea todas las posiciones y luego
imprima el doble de los valores leídos en cada una de sus posiciones.
90. Crear un programa con un vector ORIGEN de 10 posiciones que lea todas las posiciones y
luego traslade el doble de los valores en aquellas posiciones impares a otro vector
DESTINO.
a. Ejemplo dado: Vector Origen
1 1 2 2 3 4 6 7 9 5
b. Resultado Esperado: Vector Destino
1 2 2 4 3 8 6 14 9 10
91. Desarrollar un algoritmo lea desde teclado un vector de 10 posiciones y que detecte los
elementos repetidos de un vector y los reemplace con un 0 y los deposite en otro vector.
Debe mostrarse por pantalla el vector resultante
Ejemplo dado:
2 1 2 3 4 11 3 2 5 9
Resultado Esperado:
0 1 0 0 4 11 0 0 5 9
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
92. Dado un vector de 10 posiciones se debe reemplazar los valores impares con una 0
Ejemplo dado:
2 1 2 3 4 11 3 2 5 9
Resultado Esperado
2 0 2 0 4 0 0 2 0 0
93. Dado un vector de 6 posiciones se debe
a. Sumar los valores pares
b. Multiplicar los valores impares
Mostrar el resultado por pantalla.
Ejemplo dado:
2 2 2 3 1 2
Resultado Esperado: Suma: 6 Multiplicación: 12
94. Escribir un programa que lea diez números, los guarde en un vector y a
continuación los muestre por pantalla en orden inverso al de su entrada
95. Escribir un programa que lea tres números y los guarde en un vector. A continuación los
ordenará y guardará los valores ordenados en otro vector. Finalmente sacará ambos
vectores por la pantalla
96. Realizar un programa que genere 3 vectores de 10 posiciones cada uno. El primero está
formado por los números del 1 al 10. El segundo por el cuadrado de estos números y el
tercero por el cubo.
97. Realizar un programa que por medio de un menú de opciones (usar la instrucción switchcase) y trabajando con un vector de 10 números reales me permita:
a. Cargar el vector
b. Mostrar por pantalla el vector
98. Inicializar a cero una matriz de 6 filas por 5 columnas. Cargar valores enteros en un
elemento determinado, para lo cual se debe solicitar por pantalla el número de fila, de
columna donde vamos a cargar el valor, luego ingresar el valor. Una vez finalizada la
carga mostrar la matriz por filas y luego por columnas
99. Un programa registra de los 100 empleados de una fábrica su número de legajo (coincide
con el número de índice de un vector), la edad y el salario. Se pide:
a. Ingresar los datos correlativamente
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
b. Calcular la edad promedio y luego mostrar por pantalla
los números de legajo de los empleados cuya edad supere
el promedio
100. Mismo enunciado al ejercicio anterior. Se pide:
a. Ingresar los datos en forma aleatoria. (Se pide ingreso de
número de legajo de 1 a 100, se posiciona en el elemento
correspondiente y se ingresa edad y salario)
b. Calcular salario promedio informando número de legajo
de aquellos cuyo salario supere el promedio
101. Con el objeto de regular la temperatura y la humedad de un ambiente
climatizado se registran hora por hora los siguientes datos: el día, la hora, la temperatura
y la humedad.
Estos datos se ingresan sin ningún orden a un programa que luego muestra por pantalla:
a. Día por día la temperatura máxima registrada y la hora.
b. Los promedios de temperatura y de humedad de todo el
mes.
c. Considerando que la temperatura ideal oscila entre 21 y
23 grados, indicar en cuantos días la temperatura media
estuvo fuera de dicho intervalo.
Nota: El mes tiene 30 días, hay 24 juegos de datos por día.
102. En un proceso de fabricación de resistencias se dispone del valor ideal y su
tolerancia
(Porcentual) y de 90 valores medidos.
Se pide hacer un programa que:
a. Emitir un listado ordenado en forma creciente de todos
los valores que estén dentro del rango admisible.
b. Valor medido más cercano al ideal.
103. Realizar un programa que permita determinar quienes participan y el orden de
largada en una carrera automovilística.
La cantidad de inscriptos es de 58; se ingresan cuatro datos por corredor los cuales
consisten en número de coche, tiempo de vuelta, nombre del corredor y el nombre de la
escudería.
Cada participante tiene tres posibilidades para clasificar (ninguna, una o 3 vueltas de
clasificación) por lo tanto cada vez que un participante completa una vuelta se debe
realizar un ingreso de datos (puede haber más de un ingreso por competidor)
Considerándose la vuelta de menor tiempo de cada corredor y la cantidad de vueltas
realizadas para establecer la clasificación.
La numeración de los coches no es correlativa sino que puede ser cualquiera (Por ejemplo
Pueden existir los coches 70, 113, 201 etc.)
Se debe emitir un listado ordenado por tiempo de clasificación en orden creciente
informando todos los datos de los 30 mejores y a continuación otro listado similar de los
restantes competidores que hayan realizado prueba de clasificación (Si un auto no dio
ninguna vuelta no se incluye) que se utilizará para determinar el orden de los suplentes.
Nota: Finaliza la carga de datos con un número negativo en auto o cargando los 58
permitidos.
104.Escribir un programa que tras asignar los números, -2, 5, 8, -9, 10, 15 y –4 a un arreglo
calcule, independientemente, la suma de los elementos positivos y negativos.
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
Vectores Bidimensionales
105. Efectuar la carga de una matriz de 5x5 desde teclado. Efectuar la sumatoria de
valores alojados en una de sus diagonales y el promedio de los valores de la diagonal
siguiente.
Mostrar por pantalla los resultados.
106. Realizar un programa que genere 3 vectores de 10 posiciones cada uno. El
primero está formado por los números del 1 al 10. El segundo por el cuadrado de estos
números y el tercero por el cubo.
107. Realizar un programa que por medio de un menú de opciones (usar la instrucción
switch-case) y trabajando con un vector de 10 números reales me permita:
A) Cargar el vector
B) Mostrar por pantalla el vector ordenado
108. . Hacer un programa que permita leer 20 números enteros (positivos y negativos)
distintos de cero. Mostrar el vector tal como fue ingresado y luego mostrar los positivos
ordenados en forma decreciente y por último mostrar los negativos ordenados en forma
creciente.
109. . Inicializar a cero una matriz de 6 filas por 5 columnas. Cargar valores enteros en
un elemento determinado, para lo cual se debe solicitar por pantalla el número de fila,
de columna donde vamos a cargar el valor, luego ingresar el valor. Una vez finalizada la
carga mostrar la matriz por filas y luego por columnas
110. . Efectuar la carga de una matriz de 5x5 desde teclado. Efectuar la sumatoria de
los valores alojados en la diagonal principal y el promedio de los valores alojados en la
contradiagonal.
Mostrar por pantalla los resultados.
111. Efectuar la carga de una matriz de 5x3 desde teclado. Obtener como resultado la
matriz transpuesta. Mostrarla por pantalla.
Sumatoria Promedio
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
112. Efectuar la carga de dos matrices de 3x3 desde teclado. Obtener como resultado
el producto de matrices y colocarlo en una nueva matriz. Mostrar el resultado por
pantalla.
113. - Efectuar un programa en código C el cual permita la carga de una matriz de 3x3
desde teclado. A esta matriz la llamaremos MatOrigen.
Una vez cargada la matriz efectuar el análisis de los valores cargados y obtener como
resultado otra matriz la cual llamaremos MatDestino en donde como resultado deberá
alojar en la misma fila columna el valor 1 (uno) en caso de que el valor origen sea par y
un 0 (cero) en caso de que el valor sea impar. Mostrar el resultado de la matriz destino
por pantalla. Ver ejemplo ilustrativo.
MatOrigen MatDestino
114. Efectuar un programa en C el cual permita la carga de una matriz de 3x3 desde
teclado. A esta matriz la llamaremos MatOrigen.
Una vez cargada la matriz, obtener como resultado un vector de 2 posiciones al que
llamaremos VecDestino, el cual deberá alojar en la posición 0 la SUMA de los valores
1 2 5
8 3 5
3 4 4
1 0 1
0 1 1
1 0 0
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
origen Impares y en la posición 1 la suma de los valores origen pares. Mostrar el
resultado de Vecdestino por pantalla.
Ver ejemplo ilustrativo.
 MatOrigen VecDestino
115. . Un programa carga la edad de 100 alumno de una escuela, el ingreso lo realiza
por su número de legajo (coincide con el número de índice de un vector). Se pide:
C) Ingresar los datos correlativamente
D) Calcular la edad promedio y luego mostrar por pantalla los números de legajo de
los alumnos cuya edad supere el promedio
116. . Mismo enunciado al ejercicio anterior. Se pide:
E) Ingresar los datos en forma aleatoria. (Se pide ingreso de número de legajo de 1 a
100, se posiciona en el elemento correspondiente y se ingresa edad)
F) Calcular que porcentajes de alumnos superan los 18 años
117. . Hacer un programa que permita cargar 15 palabras en una matriz. Las palabras
no pueden superar las 20 letras, luego ingresar una letra y por medio de una función
buscar la primera coincidencia de la letra en cada una de las palabras. Desde main
mostrar la palabra que queda a partir de la coincidencia, por ejemplo
Si ingresamos la letra a
Holanda donde se produce la coincidencia mostramos la palabra que queda anda
Chau igual donde se produce la coincidencia mostramos la palabra que queda au
Andrea ídem a los anteriores Andrea
118. . Con el objeto de regular la temperatura y la humedad de un ambiente
climatizado se registran hora por hora los siguientes datos: el día, la hora, la temperatura
y la humedad.(Los datos se almacenan en dos matrices de 24 X 30)
Estos datos se ingresan sin ningún orden a un programa que luego muestra por pantalla:
G) Día por día la temperatura máxima registrada y la hora.
H) Los promedios de temperatura y de humedad de todo el mes.
I) Considerando que la temperatura ideal oscila entre 21 y 23 grados, indicar en
cuantos días la temperatura media estuvo fuera de dicho intervalo.
Nota: El mes tiene 30 días, hay 24 juegos de datos por día.
119. Escribir un programa que pida una contraseña y permita tres intentos. Si el
usuario da la contraseña correcta responde "Enhorabuena!" y queda inactivo, con este
mensaje. En caso contrario el programa escribe "Lo siento, contraseña equivocada" y se
cierra de inmediato.
1 2 5
8 3 5
3 4 4
17 18
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
120. Dada una matriz de 3x3 en la que se leen desde teclado valores numéricos y se
lee número de fila o columna, se pide tener un menú principal. las operaciones
 1) FILA 2) COLUMNA
 1) SUMA, 2) RESTA
Ejemplo dado:
1 1 3
2 3 2
3 5 1
Se lee desde teclado 1) Fila
Se lee desde teclado Numero de Fila : 2
Se lee desde Teclado Operación matemática 1) Suma
Resultado Esperado: 7
121. Desarrollar una matriz de 3X3. Ingresar números enteros. Solicitar al usuario que
ingrese un número de fila y obtener el número mayor ingresado en dicha fila.
122. Dada una matriz de 3X4, desarrollar un programa que solicite al usuario el ingreso
de números enteros. Al finalizar obtener la suma de todos los números positivos
ingresados y la suma de los numero negativos.
123. Desarrollar una matriz de 3X3, ingresar números enteros. Al finalizar mostrar el
mayor número ingresado y su posición.
124. Cada semana, un negocio de electrodomésticos registra las ventas de los
productos individuales que hay en existencia. Al finalizar el mes, se genera un reporte
semanal y se analizan. Un ejemplo de un mes típico se muestra en la siguiente tabla.
Semana Televisor Heladera Lavarropa
1 3 4 3
2 5 6 5
3 3 2 1
4 6 7 8
Determinar:
a) Calcular el número total de artículos vendidos cada semana
b) Calcular el número total de artículos vendidos en el mes.
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
Funciones
125. Realiza un programa principal que lea tres números enteros por teclado, los
almacene en tres variables (x, y, z) y llame a una función llamada máximo(), con tres
argumentos, que devuelva el máximo de estos tres valores.
126. Realiza un programa principal que utilice varias cadenas de caracteres y llame a
unas funciones programadas por ti que realicen las mismas tareas que strlen( ) y strcmp(
). Deberás llamar a tus funciones con un nombre distinto a las que existen en la librería
de C. Sugerimos los siguientes nombres:
strlen( ) -> cuentaCaracteres( )
strcmp( ) -> comparaCadenas( )
127. Ejecutar el ejercicio 117, creando un archivo cabecera (.h) utilizando las
funciones desarrolladas desde mis librerías cabeceras.
strlen( ) -> cuentaCaracteres( )
strcmp( ) -> comparaCadenas( )
128. Realiza un programa principal que lea una palabra desde el teclado e identifica si
la misma es palindroma (capicua). Deberás llamar a tus funciones con un nombre distinto
a las que existen en la librería de C. Sugerimos los siguientes nombres:
129. Realizar un programa principal que mediante el llamado de una función calcula el
factorial de un número entero n.
A) Mediante una estructura repetitiva
B) Mediante la siguiente fórmula recursiva: n! = n * (n-1)! Teniendo en cuenta
que: 1! = 1.
Por medio de un menú se podrá elegir si el resultado se calcula por la opción A o por la
opción B.
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
Estructuras
130. Desarrollar mediante código de programación la carga de datos de 5 alumnos y
que posea la siguiente estructura:
Codigo_Alumno INT
Apellido y Nombre CHAR(50)
Telefono Char(12)
Edad INT
Finalmente escribir la rutina que liste los datos cargados.
131. Se debe cargar las notas de un examen realizado a 15 alumnos. Realizar un
algoritmo que muestre el promedio del grupo y los nombres de los alumnos que tienen
una nota por debajo del promedio.
Matricula
Apellido
Nombre
Edad
Nota 
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
132. Escribir un programa para calcular el importe de una venta en un supermercado.
El usuario debe ingresar por pantalla el nombre del producto, el precio por unidad y el nº
de unidades vendidas y el programa sacará por pantalla:
Ordenado alfabéticamente el nombre de los producto vendidos
El nº de unidades vendidas
El subtotal de ese producto
El total general de toda la venta.
Permitir como máximo 40 productos distintos en la venta.
Se considera que primero se carga un vector con los 40 productos permitidos para la
venta y su precio
133. . Hacer un programa que ingrese por pantalla los datos de personas (Nombre y
edad). Se pide mostrar un listado ordenado alfabéticamente. Se ingresan solamente 100
personas
134. . Hacer un programa que ingrese por pantalla los datos de personas (Nombre y
edad). Se pide mostrar un listado ordenado alfabéticamente de las personas mayores a 25
años. Se ingresan solamente 100 personas
135. Se ingresa el Nombre y la fecha de nacimiento de 10 personas. Por medio de una
función se debe realizar la carga de la fecha verificando que los valores se encuentren
dentro de lo permitido y que no se ingrese un valor posterior a la fecha actual.
Terminada la carga permitir la modificación de la fecha a partir del ingreso del nombre.
Al finalizar el programa mostrar los datos ingresados ordenados por edad. Realizar una
función que permita calcular la edad a partir de la fecha de nacimiento
136. Un banco tiene 50 cuentas. Se pide hacer un programa que realice las siguientes
opciones:
i. ALTA: Permitir ingresa los siguientes datos de cada cuenta—
ii. Numero de cuenta--- entero
iii. Tipo de cuenta --- carácter (C: cuenta corriente, A: caja de ahorro)
iv. Saldo de la cuenta --- flotante
v. MODICACION: Permite cambiar el saldo de una cuenta
vi. CONSULTA: Muestra los datos de una cuenta cualquiera
137. Tomando el ejercicio anterior se pide mostrar todos los datos de las cuentas cuyo
saldo supere los $ 500, ordenado en forma creciente de saldo
138. . Se ingresa nombre, sexo y edad de 100 personas. Se pide averiguar la cantidad
de mujeres que tienen entre 20 y 30 años, cuantos hombres son menores a 37 años.
139. En un depósito se almacenan 10 tipos de piezas distintas. Al comenzar el mes se
cargan en un vector la siguiente información:
Código de pieza (De 1 a 10)
Existencia al comenzar el mes
Luego se carga los pedidos del mes creando otro vector con la siguiente información:
Código de pieza
Cantidad pedida
Si la existencia alcanza se entrega el pedido y se actualiza el saldo. Si no alcanza se
rechaza el pedido completo.
Al finalizar se emitir un listado ordenado en forma creciente por saldos finales donde se
indique:
Código de pieza, Cantidad inicial Total, Cantidad entregada Total, Cantidad total
rechazada
140. Una empresa comercializa 5 artículos en 3 sucursales. Se debe realizar un
programa que presente un menú con las siguientes opciones:
B) Carga de datos: se registrará la existencia en depósito informando N° de
sucursal, N°de artículo y cantidad
C) Venta de artículos: se registran las ventas realizadas informando N° de sucursal,
N°de artículo y cantidad vendida. Se debe verificar que la cantidad vendida no
supere la existencia, informando si la venta no se puede realizar por este motivo
D) Existencia de mercaderías: listar por pantalla saldos existentes de mercaderías
E) Salir del programa
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
141. Una fábrica tiene 30 obreros con legajos de 1 a 30. Por cada obrero se registran
los movimientos que intervienen en la liquidación de su sueldo. Los datos informados son:
N° de legajo (entero entre 1 y 30)
Sueldo básico
Bonificación por antigüedad
Se debe calcular el sueldo neto de cada obrero considerando: Un descuento por
jubilación que representa el 16 % del sueldo básico, un descuento por obra social que
representa el 4,5 % del sueldo básico. Se pide:
Listado por pantalla con el siguiente formato:
i. N° de legajo, Sueldo básico, Jubilación, Obra social, Antigüedad, Sueldo
neto
Mostrar por pantalla otro listado de los legajos con mayor bonificación por antigüedad y
el monto de dicha bonificación
Listado de todos los obreros ordenado por sueldo neto
142. Una empresa de colectivos tiene 3 líneas de 12 coches cada una. Por cada viaje el
chofer entrega al llegar a la terminal una planilla con el número de coche (de 1 a 12),
número de línea (de 1 a 3) y la recaudación del viaje. Las planillas se entregan sin ningún
orden. Se pide informar por pantalla:
F) La recaudación total por línea de colectivo
G) La recaudación total por coche
H) La recaudación total general
143. De los 100 alumnos de una escuela se ingresa: nota del 1° trimestre, nota del 2°
trimestre, nota del 3° trimestre
El orden de ingreso coincide con su número de legajo que es de 1 a 100
Luego del ingreso de los datos el programa debe preguntar al usuario si desea corregir
alguna nota, para lo cual se debe ingresar: N° de legajo, Trimestre a corregir (1,2 o 3) u
nueva nota, antes de corregir el programa debe mostrar la nota anterior correspondiente
al trimestre indicado y preguntar si confirma la modificación. Si contesta “SI” se registra
la nueva nota, caso contrario pregunta si desea continuar. Cuando no se registren más
modificaciones se deberá listar por pantalla:
N° DE LEGAJO, 1° TRIMESTRE, 2° TRIMESTRE, 3° TRIMESTRE, PROMEDIO
144. Una compañía de aviación tiene 4 destinos (numerados de 1 a 4) con 3 vuelos
cada uno (numerados de 1 a 3). Se ingresa la información de las plazas disponibles en
cada uno de los 12 vuelos. Posteriormente se ingresa por día los pedidos de pasajes, de
los que se ingresa:
N° de pedido
N° de destino
N° de vuelo
Cantidad de pasajes requeridos
Si la cantidad de pasajes pedidos es superior a la disponibilidad existente en ese vuelo y
para ese destino se rechaza el pedido indicando la leyenda: NO HAY CANTIDAD DE
PLAZAS DISPONIBLES.
Se pide informar:
la cantidad de pasajes sobrantes en cada vuelo
los N° de pedidos rechazados con la cantidad de pasajes solicitados. (Se ingresa 100
pedidos por día)
145. En un establecimiento educativo se desea realizar un cálculo estadístico y para
ello se ingresan los siguientes datos por alumno:
Número de legajo
Nombre y Apellido
Promedio de calificaciones
Se desea obtener un listado ordenado por promedio en forma descendente de todos
aquellos alumnos cuyo promedio supere o iguale el promedio general (que será solicitado
por pantalla).
El fin de datos se indica con número de legajo 0 (cero); no hay más de 100 alumnos.
146. En el problema anterior agregar la siguiente posibilidad:
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
Interrogar al operador acerca de si desea realizar consultas o no; en caso de contestar si
el programa debe solicitar el ingreso de un número de legajo y debe emitir como
respuesta los datos del alumno (Este proceso es para todos los alumnos ingresados) e
interrogar al operador nuevamente; en caso de contestar no finalizar.
147. Hacer un programa que cargue el nombre y la fecha de nacimiento de 10
personas. Mostrar por pantalla las personas que tienen más de 30 años.
148. Hacer un programa que permita cargar nombre y fecha de nacimiento de 10
personas. Una vez ingresados los datos se debe permitir al usuario realizar modificaciones
a la fecha de nacimiento. Por final de programa mostrar un listado ordenado
alfabéticamente de las personas que cumplen años el día de hoy.
149. Realizar un programa que permita cargar Nombre y nota de 30 alumnos.
Realizar funciones que validen los datos ingresados. Mostrar un listado ordenado
alfabéticamente. El ordenamiento debe ser hecho por medio de una función
150. Realizar un programa con un menú de opciones que permita hacer altas y bajas
de un máximo de 30 productos.
Los datos que se ingresan son Código, descripción, precio y cantidad. El código es una
cadena de caracteres de 5 dígitos, y la descripción no puede exceder los 15 caracteres.
El Código no puede repetirse, en el caso de ingresar un código existente se debe informar
a través de un mensaje por pantalla y no dejar ingresar el resto de los datos.
151. Hacer un programa que permita ingresar los datos de 10 empleados de una
empresa. Los datos solicitados son:
Nombre
Dirección (calle, número, localidad)
Edad
Sexo (M = masculino, F = femenino)
Luego el programa debe permitir las siguientes opciones:
I) listar los empleados menores de 25 años
J) listar los empleados que viven en Avellaneda
K) listar los empleados que viven en Avellaneda y son menores de 25 años
L) ordenar el vector por nombre y listarlo
152. . Una playa de estacionamiento registra la patente y la hora cada vez que se
produce la llegada de un vehículo. Realizar el programa que permita realizar la carga de
los datos y calcule el importe de la estadía cuando el vehículo sale del estacionamiento.
La playa tiene lugar para 40 coches.
153. En un comercio se carga el Nombre de medicamento, precio y cantidad de 50
artículos. Hacer un programa que me permita ingresar los datos y luego mostrar ordenado
por nombre los artículos que tienen menos de 10 unidades en stock.
154. Una empresa periodística publica 8 tipo de revistas. Se registra para cada una de
ellas los siguientes datos:
Número de revista (De 1 a 8)
Cantidad de ejemplares vendidos
Valor de venta de la revista
Monto cobrado por publicidad
Gastos de edición
Se pide emitir un listado por pantalla ordenado de mayor a menor por la ganancia de la
publicación con el siguiente formato:
N°revista, cantidad de Ejemplares vendidos, Recaudado por ventas, Cobrado por
publicidad, Gastos de edición, Ganancia de la publicación
Se entiende que la ganancia de la publicación se calcula de multiplicar la cantidad de
ejemplares vendidos por el valor de venta de la revista más el monto cobrado por
publicidad menos el gasto de edición.
155. Crear una estructura llamada "jugador", que almacene la siguiente información
sobre jugadores de fútbol:
Nombre del jugador
Nombre del equipo
Cantidad de partidos jugados
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
Cantidad de goles convertidos
Promedio de goles por partido
Utilizando la estructura, declarar un arraya de 100 elementos de estructuras de ese tipo.
Escribir un programa que cargue los datos del array. Se ingresan: nombre del jugador,
nombre del equipo, cantidad de partidos jugados y cantidad de goles convertidos. Por
cada jugador se calcula el promedio de goles por partido(es dividir la cantidad de goles
convertido por partidos jugados) y se almacena en el campo correspondiente a cada
elemento del arraya. Luego de este proceso se debe mostrar por pantalla la información
contenida en el arraya (nombre del jugador, nombre del equipo, cantidad de partidos
jugados, cantidad de goles convertidos y promedio de goles por partido.
156. Armar 2 estructuras una de productos y otra de clientes
La estructura de productos:
Código
Precio unitario
Descripción
Stock
La estructura de clientes:
Código
Nombre
Monto acumulado de la compra
Hacer un programa que muestre las siguientes opciones en pantalla:
CARGA: Permite cargar los productos (100) o los clientes (45). Verificar la no existencia
de ese cliente o ese producto.
FACTURACION: Ingresando código del cliente, código del producto y cantidad vendida,
confeccionar la factura correspondiente realizando los siguientes controles:
Verificar que cantidad vendida no supere la existencia. Restarle al stock la cantidad
vendida. Actualizar monto acumulado de compras de cada cliente. Mostrar por pantalla la
factura
LISTADO: listar los clientes cuyo monto acumulado de compras supere $1000
SALIR: Permite finalizar el programa
157. Realizar una agenda para guardar los datos de hasta 200 personas de las cuales
se toman los siguientes datos:
Nombre
Apellido
Dirección
Localidad
Código Postal
Fecha de nacimiento (Día, Mes y Año)
Utilizar estructuras anidadas.
Se pide que una vez cargados los datos, se disponga de un menú de opciones que me
permita hacer las siguientes consultas
1) Consulta por apellido
2) Consulta por localidad
3) Consulta por Año de nacimiento
4) Consulta por cumpleaños
158. Realizar un programa que permita ingresar los datos de 50 electrodomésticos,
estos son:
Código del electrodoméstico (numero entero de 5 caracteres)
Descripción (letra de 20 caracteres)
Modelo (letra de 10 caracteres)
Precio
Para la carga se debe realizar una función que valide la no repetición del código.
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
Terminada la carga se debe permitir al usuario realizar modificaciones en el precio de
distintos productos. Para ello se ingresa el código y la descripción, si se encuentra se
ingresa el nuevo precio, caso contrario se debe mostrar un mensaje “No se encontró el
producto” y mostrar un listado con los códigos y descripciones cargadas de forma que el
usuario tenga una ayuda.
Finalmente y por medio de funciones se deben mostrar por pantalla los siguientes
resultados.
M) Ordenar los datos por código y mostrarlos
N) Ordenar los datos por modelo y mostrarlos
O) Ingresar las 2 primeras letras del modelo y mostrar código y modelo de los
productos que cumplan con la condición.
P) Ingresar descripción y modelo del equipo, luego mostrar todos los
electrodomésticos que coincidan
159. Para el sector de ventas de una empresa se necesita realizar un programa con las
siguientes características:
Realizar modulo que cargue los 10 productos que existen para la venta, los datos que se
ingresan para cada producto son los siguientes:
Código de artículo 6 caracteres de longitud y el primer carácter debe ser X
Descripción Puede tener un máximo de 20 caracteres
Precio del producto
Cantidad en existencia del producto
También se debe hacer otro modulo que guarde las ventas realizadas en un día, los datos
que se guardan son:
Código de artículo
Cantidad vendida
Fecha de venta
No se realizan más de 100 ventas por día.
El programa debe contar con un menú con las siguientes opciones:
Q) Carga de productos,
R) Venta de productos
Salir del programa
Carga de productos: Se ingresan los datos relativos a cada producto y se guarda en el
vector correspondiente. Finaliza la carga cuando se ingresa cero en código de producto o
se ingresaron los 10 productos permitidos
Venta de productos: Se ingresa el código de producto que se desea vender y la cantidad
a vender. Se busca el código en el vector de carga de producto y se verifica que la
cantidad existente es suficiente para realizar la venta. Si no existe el producto enviar un
mensaje, lo mismo si la cantidad vendida supera la cantidad existente del producto.
Luego se guarda en el vector correspondiente dicha venta Por otra parte se debe
actualizar la cantidad existente en el vector de productos cargados.
Tanto la carga de producto como la venta finalizan cuando se ingresa cero en código de
artículo o en venta supera 100.
Salir del programa: Mostrar ordenado por código de producto el total de ventas general y
el total de productos vendidos sacar también el promedio de cantidad vendida por
producto.
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
160. Hacer un programa que permita cargar 20 productos. Los datos que se solicitan
son: código, Modelo (máximo 10 char), cantidad y precio.
El código debe ser generado en forma automática por medio del programa. Se pide que
por medio de un menú de opciones el programa resuelva lo siguiente:
S) Carga de productos
T) Venta de productos
U) Modificación del precio
V) Modificar el modelo
W) Listar ordenado por código
X) Listar ordenado por código los productos con menos de 3 unidades
161. . Realizar un programa que permita cargar las operaciones realizadas en un
cajero. Los datos que se deben obtener por cada operación son:
Nombre: Nombre del cliente
Monto: Cantidad de dinero de la operación bancaria
Operación: Depósito o extracción
Fecha de la operación (No la ingresa el usuario, se toma en forma automática)
Hora de la operación (No la ingresa el usuario, se toma en forma automática)
Terminada la carga de datos (máximo 100 operaciones diarias) se debe mostrar el detalle
de un día ordenado alfabéticamente y el saldo total
Si se desea que el programa muestre el detalle de las operaciones de un cliente en un
mes
162. Una empresa controla con un programa los movimientos de mercadería que tiene
diariamente en su depósito, para ello el programa informa los siguientes:
CODIGO DE ARTICULO (de 1 a 30, caso contrario informar error y volver a Ingresar, ya que
solo tiene 30 productos disponible)
DESCRIPCION DEL ARTÍCULO
CANTIDAD EN STOCK
PRECIO UNITARIO
OPCION A: registrar compras y ventas, para lo cual se debe informar:
CODIGO DE ARTICULO (de 1 a 30, caso contrario informar error y volver a ingresar)
CODIGO DE MOVIMIENTO (1 o 2 caso contrario se informa error vuelve a ingresar). El
código 1 indica una venta y 2 una compra.
CANTIDAD (comprada o vendida de acuerdo al código del movimiento)
Preparar esta opción para que se continúe trabajando con ella hasta que el usuario
decida terminar y pasar al menú principal.
OPCION B: consultar el precio unitario de un artículo.
El usuario ingresa el Código de artículo (de 1 a 30, caso contrario informar error y volver
a ingresar) y el sistema le mostrar la descripción del artículo y el precio unitario.
OPCION C: consultar existencia en stock de un artículo.
El usuario ingresa el Código de artículo (de 1 a 30, caso contrario informar error y volver
a ingresar) y el sistema le mostrar la descripción del artículo y la existencia.
OPCION D: finalizar la ejecución del programa.
163. En una biblioteca se registran los libros existentes, informándose:
CODIGO DE LIBRO (De 1 a 100, caso contrario informar error y volver a ingresar)
CANTIDAD DE EJEMPLARES
AUTOR
TITULO
El programa muestra por pantalla las siguientes opciones:
OPCION 1: ALTA DE LIBROS--El ingreso de libros nuevos o el primer ingreso de libros.
Considerar que el código de libro debe estar entre 0 – 100 sino mostrar un mensaje de
error)
OPCION 2: PRESTAMOS DE LIBROS-- El bibliotecario deberá ingresar:
CODIGO DE LIBRO (De 1 a 100, caso contrario informar error y volver a ingresar). El
programa le mostrar el autor y el título y sólo se prestar el libro si la cantidad de
ejemplares es mayor a 1. Si el usuario confirma el préstamo restar 1 a la cantidad de 
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
ejemplares. Si la cantidad de ejemplares es 1, se mostrar la leyenda "Solo queda el
ejemplar de lectura en sala" y no se registrar el préstamo.
OPCION 3: DEVOLUCION DE LIBROS--- El bibliotecario deberá ingresar:
CODIGO DE LIBRO (de 1 a 100, caso contrario informar error y volver a ingresar). El
programa le mostrar el autor y el título y si el usuario confirma, registrar la devolución
sumando 1 a la cantidad de ejemplares.
OPCION 4: FIN
Significa que finaliza el programa pero antes debe informar lo siguiente:
Listado ordenado en forma decreciente de los libros prestado en ese día. El listado
contendrá lo siguiente: TITULO AUTOR CANTIDAD DE VECES PRESTADO
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
Archivos de texto y Binarios
164. Dado un fichero de texto, copiar el contenido del mismo en otro archivo con
diferente nombre.
El nombre del fichero origen y destino debe ingresarse desde teclado y se deberá validar que el
existan (ambos, origen y destino). Luego procederá a efectuar la copia del contenido de un
archivo en el otro.
165. Realizar un programa principal que mediante el llamado de funciones y archivos
permita en un archivo BINARIO:
 Agregar un contacto
 Mostrar todos los contactos
 Eliminar un contacto
 Consultar un contacto por DNI
 Modificar un contacto
La estructura de registros a agendar es la siguiente :
DNI INT ,
Apellido CHAR(50),
Nombre Char(50)
Telefono Char(20)
El archivo sobre el que se guardará la información se llamará contacto.dat
166. Efectuar un programa en código C el cual ejecute una rutina que lea y depure un
archivo de texto ya existente.
El archivo de texto se aloja en el la unidad C: y su nombre es Parcial.txt.
El contenido del archivo es una cadena de este tipo
“de*bo dep*urar la cadena eliminando el cará*cter asterisco”
El resultado de la rutina deberá crear otro archivo en la unidad C: y su nombre será
Parcialdepurado.txt. Este deberá tener la cadena depurada. 
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
Resultado esperado: “debo depurar la cadena eliminando el carácter asterisco”
167. Efectuar un programa en código C el cual ejecute una rutina que lea y examine
el contenido de un archivo de texto ya existente. Al examinar el contenido deberá
contabilizar la cantidad de asteriscos que hay en el mismo. El archivo de texto se aloja
en el la unidad C: y su nombre es Parcial.txt. El contenido del archivo es una cadena de
este tipo
“de*bo conta*bilizar el cará*cter asterisco”
El resultado de la rutina deberá crear otro archivo en la unidad C: y su nombre será
Parcialcontabilizado.txt. Este deberá tener la cadena con el resultado de la cantidad de
asteriscos. Resultado esperado: “Se han encontrado 3 asteriscos”
168. Realizar un programa en código C que ejecute una rutina para que lea y examine
el contenido de un archivo de texto ya existente. El archivo de texto esta en la unidad c:
y su nombre es archivo.txt, el contenido del archivo es una cadena que dice:
Remp1azar e1 numero 1 por 1a 1etra L
El resultado de la rutina deberá crear otro archivo en la unidad C: llamado resultado.txt y
deberá contener la siguiente cadena: RempLazar eL numero L por La Letra L
169. Se desea obtener una estadística de un archivo de caracteres ya existente. El
mismo se llama carta.txt. Escribir un programa para contar e informe el número de
palabras de que consta un archivo, así como la longitud promedio en caracteres.
Ejemplo carta.txt
“Esta es una carta de ejemplo
y vamos a contar las palabras”
Resultado deberá analizar el texto y mostrara por pantalla:
“Esta(4) es(2) una(3) carta(5) de(2) ejemplo(7)
y(1) vamos(5) a(1) contar(5) las(3) palabras(8)”
“Se han contabilizado 12 palabras el archivo. La longitud promedio es de 3.83 caracteres”
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
Búsqueda y Ordenamiento
170. Ordena un conjunto de números enteros almacenados en un vector, utilizando el
método de la burbuja.
Ejercicio 3.2- Ingresar dos vectores de 3 posiciones desde teclado. Sumar las posiciones
coincidentes [1,1], [2,2], [3,3] y retornar el resultado mediante las siguientes opciones.
A) Método de la burbuja
B) Método de Selección
C) Método de Inserción
171. Realizar la carga de un vector de 10 posiciones.
Mediante un menú ordenarlo y cargarlo en otro vector.
A) Método de la burbuja
B) Método de Selección
C) Método de Inserción
Luego de tener el vector resultado ordenado, ingresar x teclado el valor clave a buscar y permitir
que el programa busque mediante búsqueda secuencial, o mediante búsqueda binaria.
172. - Realizar un programa que le agregue estas nuevas funcionalidades al ejercicio
1.10 y que mediante el llamado de funciones y archivos permita:
a) Eliminar un contacto,
b) Modificar un Contacto,
c) Buscar y mostrar un contacto x DNI
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
Ejercicios Integradores
Integrador arreglos y funciones
173. Desarrollar un algoritmo mediante la implementación de vectores de hasta 10
posiciones, que simule el juego del “Ahorcado”. Se lee la palabra desde teclado y se
proveen 10 opciones.
Ejemplo:
 XXXXXXXXXX
Integrador(Archivos secuenciales, Ordenamiento simple,
Búsqueda Binaria)
174. Efectuar un programa en código C el cual ejecute una rutina que lea y depure un
archivo de texto ya existente. El archivo de texto se aloja en el la unidad C: y su nombre
es tecladoqwerty.txt. El contenido del archivo es una cadena la cual contiene solo las
letras del teclado en una disposición QWERTY. (ver Imagen)
El contenido del archivo tiene errores en su contenido. Esos errores están reflejado en caracteres
erróneos identificados como * (asteriscos).
Ejemplo contenido del archivo (tecladoqwerty.txt.).
qw*ert*yuiop asdfg*hjkl zx*cvbnm
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
El algoritmo deberá ser capaz de:
1) Recorrer y depurar el archivo tecladoqwerty.txt eliminando los asteriscos y espacios
y cargando todos los elementos aptos a un vector de caracteres llamado abecedario.
2) Luego deberá ser capaz de informar la cantidad de caracteres Aptos y la cantidad de
caracteres erróneos. Los caracteres erróneos son los *. Los caracteres Aptos son solo
las letras.( No espacios. No asteriscos.)
3) Una vez depurado e informado los elementos Aptos y Erróneos, el algoritmo el
ordenara alfabéticamente los caracteres depurados y almacenados en el arreglo
abecedario, mediante algún método de ordenamiento simple y dará la opción a que
el usuario busque una letra especifica utilizando el método de búsqueda binaria para
certificar si la letra existe o no existe.
Ejemplo salida por pantalla
P.Lagomarsino-I.Castillo-G.Tulian-Version 2020.01
Integrador archivos binarios y funciones
175. Una empresa de micros realiza viajes diarios desde Buenos Aires (BUE) a Rosario
(ROS). El micro cuenta con capacidad para 15 pasajeros. Los pasajeros abonan el boleto
al subir al micro. El precio varía según donde ascienden. Realizar lo siguiente en código
C:
Se debe almacenar los siguientes datos del pasajero:
Nro_Boleto
Apellido
DNI
Origen
Destino
Importe
a) Crear un procedimiento que imprima un menú:
b) Altas de los datos
c) Imprimir por pantalla lo que pagó cada pasajero
d) Realizar una función que devuelva el número de asiento del pasajero que pagó el boleto
más caro e imprimir por pantalla
e) Realizar una función que devuelva el número de asiento del pasajero que pagó el boleto
más barato e imprimir por pantalla.
f) Realizar una función que permita cambiar el valor que pagó el primer pasajero, el nuevo
valor es de $300. Imprimir antes de cambiar el valor el importe abonado.