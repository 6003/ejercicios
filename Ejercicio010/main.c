// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador
#define MULTIPLO 3

int main()
{
    // Hacer un algoritmo que ingrese dos n�meros y determinar si alguno de los dos n�meros es divisible por 3.

    // Seecion de declaracion de variables locales
    int numero1;
    int numero2;
    int resto1;
    int resto2;

    // Entrada
    printf("Ingrese un numero entero: ");
    scanf("%i",&numero1);

    printf("Ingrese un segundo numero entero: ");
    scanf("%i",&numero2);

    // Proceso
    resto1 = numero1 % MULTIPLO;
    resto2 = numero2 % MULTIPLO;

    if(resto1 == 0 || resto2 == 0){
        // Salida
        printf("Por lo menos un numero ingresado es divisible por %i.\n", MULTIPLO);
    }else{
        // Salida
        printf("Ningun numero ingresado es divisible por %i.\n", MULTIPLO);
    }

    // fin del programa
    return 0;
}
