// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Realizar un algoritmo que permita leer dos n�meros,
    // determinar cu�l de los dos n�meros
    // es el menor y mostrarlo por pantalla con el mensaje
    // �El numero N es menor al numero X�

    // Seecion de declaracion de variables locales
    float num1;
    float num2;

    // Entrada
    printf("Ingrese primer numero: ");
    scanf("%f", &num1);

    printf("Ingrese segundo numero: ");
    scanf("%f", &num2);

    // Proceso
    if(num1 < num2){
        // Salida
        printf("El n�mero %.2f es menor que el numero %.2f\n",num1,num2);
    }else if(num2 < num1){
        // Salida
        printf("El n�mero %.2f es menor que el numero %.2f\n",num2,num1);
    }else{
        // los numero son iguales
        // Salida
        printf("Los dos numeros ingresados son iguales.\n");
    }

    // fin del programa
    return 0;
}
