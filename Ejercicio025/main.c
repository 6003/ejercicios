// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Realizar un algoritmo que pida un n�mero
    // y saque por pantalla su tabla de multiplicar.

    // Seecion de declaracion de variables locales
    int numero;
    int x;

    // Entrada
    printf("Ingrese un numero entero del 1 al 10: ");
    scanf("%i", &numero);

    // Proceso
    for(x=1;x<=10;x++){
        // Salida
        printf("%i X %i = %i\n",numero,x,numero*x);
    }

    // fin del programa
    return 0;
}
