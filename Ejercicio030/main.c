// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Hacer un algoritmo que se Ingresan las notas de 20 alumnos.
    // Mostrar por pantalla la
    // mejor nota.

    // Seecion de declaracion de variables locales
    float nota;
    float mejorNota = 0;
    int x;  // variable para el contador

    // Entrada
    for(x=0;x<20;x++){
        printf("Ingrese la nota (repeticion: %i): ", x+1);
        scanf("%f", &nota);

        // Proceso
        if(mejorNota < nota){
            // guardo la mejor nota
            mejorNota = nota;
        }
    }

    // Salida
    printf("La mejor nota ingresada es: %.2f\n",mejorNota);

    // fin del programa
    return 0;
}
