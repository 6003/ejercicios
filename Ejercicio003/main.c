// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador
#define IVA 21

int main()
{
    printf("Hacer un programa que ingresas un precio y calculas el iva (21%%).\nMostrar el resultado por pantalla.\n");

    // Seecion de declaracion de variables locales
    float precio;
    float iva = IVA;
    float por = 100;
    float resultado;

    // Entrada
    printf("Ingrese precio $: "); scanf("%f",&precio);

    // Proceso
    resultado = precio * (iva / por);

    // Salida
    printf("El 21%% de IVA del precio $%.2f es: $%4.2f",precio,resultado);

    // fin del programa
    return 0;
}
