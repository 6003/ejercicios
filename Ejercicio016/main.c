// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador
#define PORCENTAJE 1.1 // representa el 10% sumado

int main()
{
    // Hacer un algoritmo que se ingresa un precio( n�mero decimal)
    // y le calcula un incremento de 10%
    // Mostrar por pantalla el resultado

    // Seecion de declaracion de variables locales
    float precio;
    float resultado;

    // Entrada
    printf("Ingrese un precio: ");
    scanf("%f", &precio);

    // Proceso
    resultado = precio * PORCENTAJE; // multiplico para que me sume el 10%

    // Salida
    printf("\nEl precio %.2f mas el 10%% es igual a: %.2f\n",
           precio, resultado);

    // fin del programa
    return 0;
}
