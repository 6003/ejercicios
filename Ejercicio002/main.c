// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador
#define Anios23 23

int main()
{
    // Hacer un programa que ingresas la edad de una persona y calculas cuantos a�os va a tener dentro de 23 a�os.
    // Mostrar el resultado por pantalla.

    // Seecion de declaracion de variables locales
    int edad;
    int resultado;

    // Entrada
    printf("Ingrese edad: "); scanf("%i",&edad);

    // Proceso
    resultado = edad + Anios23;

    // Salida
    printf("La persona de %i de edad en %i a�os va a tener %i a�os.",edad,Anios23,resultado);

    // fin del programa
    return 0;
}
