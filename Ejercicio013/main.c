// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador
#define NUM_SUMA 15
#define NUM_DIVIDE 2

int main()
{
    // Hacer un algoritmo que ingrese un n�mero decimal, s�male 15 y divid� por 2 mostrar el resultado por pantalla en formato decimal

    // Seecion de declaracion de variables locales
    float numero;
    float resultado;

    // Entrada
    printf("Ingrese un numero decimal: ");
    scanf("%f", &numero);

    // Proceso
    resultado = (numero + NUM_SUMA) / NUM_DIVIDE;

    // Salida
    printf("\nEl resultado de (%.2f + %i) / %i es = %.2f\n", numero, NUM_SUMA, NUM_DIVIDE, resultado);

    // fin del programa
    return 0;
}
