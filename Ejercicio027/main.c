// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Hacer un algoritmo que se ingresan 50 n�meros enteros,
    // calcular cu�ntos n�meros
    // impares se ingresaron

    // Seecion de declaracion de variables locales
    int numero;
    int x;
    int impares = 0;

    // Entrada
    for(x=0;x<50;x++){
        printf("Ingrese un numero entero (repeticion: %i): ", x+1);
        scanf("%i", &numero);

        // Proceso
        if(numero % 2 == 1){
            // el numero es impar, le sumo 1 al contador
            impares++;
        }
    }

    // Salida
    printf("La cantidad de numero impares ingresados son %i\n",impares);

    // fin del programa
    return 0;
}
