// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Dise�ar un algoritmo que calcule la superficie de un tri�ngulo
    // a partir del ingreso de su
    // base y altura y muestre por pantalla el resultado,
    // con el mensaje
    // �La superficie del tri�ngulo es:�.�.

    // Seecion de declaracion de variables locales
    float base;
    float altura;
    float superficie;

    // Entrada
    printf("Ingrese base del triangulo: ");
    scanf("%f", &base);

    printf("Ingrese altura del triangulo: ");
    scanf("%f", &altura);

    // Proceso
    // calculo de la superfici, formula => (base*altura)/2
    superficie = (base*altura)/2.0;

    // Salida
    printf("La superficie del tri�ngulo es: %.2f",superficie);

    // fin del programa
    return 0;
}
