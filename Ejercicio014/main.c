// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Un se�or tiene 10 pares de zapatillas,
    // esa cantidad la ingresa a su computadora,
    // en el momento del ingreso le llegan 5 pares m�s,
    // que debe ingresar tambi�n en la computadora.
    // Mostrar por pantalla cuantas zapatillas tiene en total 15

    // Seecion de declaracion de variables locales
    int numero1;
    int numero2;
    int resultado;

    // Entrada
    printf("Ingrese lacantidad de 10: ");
    scanf("%i", &numero1);

    printf("Ingrese lacantidad de 5: ");
    scanf("%i", &numero2);

    // Proceso
    resultado = numero1 + numero2;

    // Salida
    printf("\nEl resultado de la suma de %i mas %i es = %i\n",
           numero1, numero2, resultado);

    // fin del programa
    return 0;
}
