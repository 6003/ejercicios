// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Hacer un algoritmo que ingresa 2 letras, mostrarlas por pantalla ordenadas alfabéticamente.

    // Seecion de declaracion de variables locales
    char caracter1;
    char caracter2;
    char aux;

    // Entrada
    printf("Ingrese dos letra (y presione enter): ");
    caracter1 = getchar();
    caracter2 = getchar();

    // Proceso
    if( caracter1 > caracter2){
        // este proceso las ordena alfabeticamente si estan alrevez.
        aux = caracter1;
        caracter1 = caracter2;
        caracter2 = aux;
    }

    // Salida
    printf("\nMostrando las letras ordenadas alfabeticamente: %c; %c.\n", caracter1, caracter2);

    // fin del programa
    return 0;
}
