// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Hacer un algoritmo que nos permita introducir un n�mero,
    // luego muestre por pantalla el mensaje �Este n�mero es positivo� o �Este n�mero es negativo�

    // Seecion de declaracion de variables locales
    float numero;

    // Entrada
    printf("Ingrese un numero: ");
    scanf("%f",&numero);

    // Proceso
    if(numero >= 0){
        // Salida
        printf("El numero %g es positivo.\n",numero);
    }else{
        // Salida
        printf("El numero %g es negativo.\n",numero);
    }

    // fin del programa
    return 0;
}
