// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Hacer un algoritmo donde se Ingresan 10 n�meros enteros.
    // Mostrar por pantalla el
    // n�mero m�s grande ingresado y en qu� posici�n se ingres�.

    // Seecion de declaracion de variables locales
    int numero;
    int x;
    int mayor = 0;
    int pos = 0;

    // Entrada
    for(x=0;x<10;x++){
        printf("Ingrese un numero entero (repeticion: %i): ", x+1);
        scanf("%i", &numero);

        // Proceso
        if(mayor < numero){
            // guardo el numero mayor y su posicion
            mayor = numero;
            pos = x +1;
        }
    }

    // Salida
    printf("El numero mayor ingresado es: %i\n",mayor);
    printf("La posicion del numero mayor es %i\n",pos);

    // fin del programa
    return 0;
}
