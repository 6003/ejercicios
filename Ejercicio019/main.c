// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    //  Hacer un algoritmo que se ingresa tres n�mero.
    // Mostrar por pantalla si est�n ordenados de menor a mayor

    // Seecion de declaracion de variables locales
    float numero1;
    float numero2;
    float numero3;

    // Entrada
    printf("Ingrese un numero: ");
    scanf("%f", &numero1);

    printf("Ingrese un numero: ");
    scanf("%f", &numero2);

    printf("Ingrese un numero: ");
    scanf("%f", &numero3);

    // Proceso
    if( numero1 < numero2 && numero2 < numero3 ){
        printf("\nLos numero ingresados %g, %g, %g estan ordenados de menor a mayor\n",
           numero1,numero2,numero3);
    }else{
        printf("\nLos numero ingresados %g, %g, %g NO estan ordenados de menor a mayor\n",
           numero1,numero2,numero3);
    }

    // fin del programa
    return 0;
}
