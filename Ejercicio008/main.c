// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador
#define MULTIPLO 2

int main()
{
    // Hacer un algoritmo que pida un n�mero entero y determine si es m�ltiplo de 2.
    // Muestra un mensaje por pantalla

    // Seecion de declaracion de variables locales
    int numero;
    int resto;

    // Entrada
    printf("Ingrese un numero entero: ");
    scanf("%i",&numero);

    // Proceso
    resto = numero % MULTIPLO;
    if(resto == 0){
        // Salida
        printf("El numero %i SI es multiplo de %i.\n",numero, MULTIPLO);
    }else{
        // Salida
        printf("El numero %i NO es multiplo de %i.\n",numero, MULTIPLO);
    }

    // fin del programa
    return 0;
}
