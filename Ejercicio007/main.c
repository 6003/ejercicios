// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Hacer un algoritmo que nos permita introducir un n�mero,
    // luego muestre por pantalla el mensaje �Este n�mero es par� o �este n�mero es impar�

    // Seecion de declaracion de variables locales
    int numero;
    int resto;

    // Entrada
    printf("Ingrese un numero entero: ");
    scanf("%i",&numero);

    // Proceso
    resto = numero % 2;
    if(resto == 0){
        // Salida
        printf("El numero %i es par.\n",numero);
    }else{
        // Salida
        printf("El numero %i es impar.\n",numero);
    }

    // fin del programa
    return 0;
}
