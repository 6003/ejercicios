// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Hacer un algoritmo que se ingresan 20 n�meros enteros,
    // calcular cu�ntos n�meros
    // impares y cu�ntos n�meros pares fueron ingresados

    // Seecion de declaracion de variables locales
    int numero;
    int x;
    int impares = 0;
    int pares = 0;

    // Entrada
    for(x=0;x<20;x++){
        printf("Ingrese un numero entero (repeticion: %i): ", x+1);
        scanf("%i", &numero);

        // Proceso
        if(numero % 2 == 1){
            // el numero es impar, le sumo 1 al contador
            impares++;
        }else{
            // el numero es par, le sumo 1 al contador
            pares++;
        }
    }

    // Salida
    printf("La cantidad de numero impares ingresados son %i\n",impares);
    printf("La cantidad de numero pares ingresados son %i\n",pares);

    // fin del programa
    return 0;
}
