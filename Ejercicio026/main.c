// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Hacer un algoritmo que se ingresan 100 n�meros enteros,
    // mostrar por pantalla la
    // cantidad de n�meros pares que se ingresaron.

    // Seecion de declaracion de variables locales
    int numero;
    int x;
    int pares = 0;

    // Entrada
    for(x=0;x<100;x++){
        printf("Ingrese un numero entero (repeticion: %i): ", x+1);
        scanf("%i", &numero);

        // Proceso
        if(numero % 2 == 0){
            // el numero es par, le sumo 1 al contador
            pares++;
        }
    }

    // Salida
    printf("La cantidad de numero pares ingresados son %i\n",pares);

    // fin del programa
    return 0;
}
