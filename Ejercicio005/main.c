// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador
#define CHICOS 3

int main()
{
    // Hacer un programa que ingresas un n�mero que representa cierta cantidad de una fruta y divid�s esa cantidad por 3 chicos.
    // Mostrar por pantalla el resultado

    // Seecion de declaracion de variables locales
    float frutas;
    int chicos = CHICOS;
    float resultado;

    // Entrada
    printf("Ingrese cantidad de una fruta: ");
    scanf("%f",&frutas);

    // Proceso
    resultado = frutas / chicos;

    // Salida
    printf("La cantidad de fruta %.2f dividida entre %i chicos, le toca a cada uno: %.2f",frutas,chicos,resultado);

    // fin del programa
    return 0;
}
