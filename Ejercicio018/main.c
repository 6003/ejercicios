// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Hacer un algoritmo que se ingresa el tiempo
    // registrado de dos autos que corrieron una carrera.
    // El tiempo se guard� como n�mero decimal.
    // Mostrar por pantalla cual llego primero

    // Seecion de declaracion de variables locales
    float tiempo1;
    float tiempo2;
    float aux;

    // Entrada
    printf("Ingrese el tiempo del primer auto: ");
    scanf("%f", &tiempo1);

    printf("Ingrese el tiempo del segundo auto: ");
    scanf("%f", &tiempo2);

    // Proceso
    if( tiempo1 == tiempo2 ){
        printf("\nLos dos autos llegaron al mismo tiempo: %.2f\n",
           tiempo1);
    }else{
        if( tiempo1 < tiempo2 ){
            // Salida
            printf("\nEl primer auto llego primero en el tiempo: %.2f\n", tiempo1);
            printf("\nEl segundo auto llego segundo en el tiempo: %.2f\n", tiempo2);
        }else{
            // Salida
            printf("\nEl segundo auto llego primero en el tiempo: %.2f\n", tiempo2);
            printf("\nEl primer auto llego segundo en el tiempo: %.2f\n", tiempo1);
        }
    }

    // fin del programa
    return 0;
}
