// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>
// agrego libreria para obtener el valor de PI
#include <math.h>

// seccion del precopilador

int main()
{
    // Dise�ar un algoritmo que
    // calcule la longitud de la circunferencia
    // y el �rea del c�rculo.
    // Considerar que por pantalla se ingresa
    // el radio de dicha circunferencia.

    // Seecion de declaracion de variables locales
    float radio;
    float area;
    float circunferencia;

    // Entrada
    printf("Ingrese el radio del circulo: ");
    scanf("%f", &radio);

    // Proceso
    area = M_PI * pow(radio , 2.0);
    circunferencia = M_PI * (radio * 2.0);

    // Salida
    printf("El area del circulo es %.2f\n",area);
    printf("La circunferencia del circulo es %.2f\n",circunferencia);

    // fin del programa
    return 0;
}
