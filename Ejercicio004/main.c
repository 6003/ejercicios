// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador
#define NUEVE 9

int main()
{
    // Hacer un programa que ingresas un precio y calculas un descuento del 9%%.
    // Mostrar por pantalla el resultado.

    // Seecion de declaracion de variables locales
    float precio;
    float descuento = NUEVE;
    float por = 100;
    float resultado;

    // Entrada
    printf("Ingrese precio $: ");
    scanf("%f",&precio);

    // Proceso
    resultado = precio * (descuento / por); // Calculo el descuento y lo guardo en resultado
    resultado = precio - resultado; // Resto al precio el descuento (reutilizo la variable de resultado para guardar el nuevo valor

    // Salida
    printf("Aplicando un descuento del %.2f%% al precio $%.2f obtenemos: $%4.2f",descuento,precio,resultado);

    // fin del programa
    return 0;
}
