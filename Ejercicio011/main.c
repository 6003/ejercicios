// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Hacer un algoritmo que ingresa una letra, luego mostrarla por pantalla.

    // Seecion de declaracion de variables locales
    char caracter;

    // Entrada
    printf("Ingrese una letra: ");
    caracter = getchar();

    // Proceso

    // Salida
    printf("\nLa letra ingresada es: %c.\n", caracter);

    // fin del programa
    return 0;
}
