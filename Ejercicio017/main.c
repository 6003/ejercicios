// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Hacer un algoritmo que se ingresa dos precios(numero decimal)
    // Mostrar los precios ordenado de mayor a menor

    // Seecion de declaracion de variables locales
    float precio1;
    float precio2;
    float aux;

    // Entrada
    printf("Ingrese un precio 1: ");
    scanf("%f", &precio1);

    printf("Ingrese un precio 2: ");
    scanf("%f", &precio2);

    // Proceso
    if( precio1 == precio2 ){
        printf("\nEl precio ingresado en los dos casos es: %.2f\n",
           precio1);
        printf("No se pueden ordenar. Son iguales!\n");
    }else{
        if( precio1 < precio2 ){
            aux = precio1;
            precio1 = precio2;
            precio2 = aux;
        }

        // Salida
        printf("\nEl precio mayor es: %.2f\n", precio1);
        printf("\nEl precio menor es: %.2f\n", precio2);
    }

    // fin del programa
    return 0;
}
