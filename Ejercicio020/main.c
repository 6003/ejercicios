// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Hacer un algoritmo que permita ingresar tres n�meros enteros y,
    // si el primero de ellos es negativo,
    // calcular el producto de los tres,
    // en caso contrario calcular la suma de ellos.

    // Seecion de declaracion de variables locales
    int numero1;
    int numero2;
    int numero3;
    int resultado;

    // Entrada
    printf("Ingrese un 1� numero: ");
    scanf("%i", &numero1);

    printf("Ingrese un 2� numero: ");
    scanf("%i", &numero2);

    printf("Ingrese un 3� numero: ");
    scanf("%i", &numero3);

    // Proceso
    if( numero1 < 0) { // si es menor a cero, el numero es negativo
        // tengo que hacer la multiplicacion (producto)
        resultado = numero1 * numero2 * numero3;

        // Salida
        printf("\nEl resultado del producto de %i * %i * %i = %i\n",
           numero1,numero2,numero3,resultado);

    }else{
        // Tengo que hacer la suma
        resultado = numero1 + numero2 + numero3;

        // Salida
        printf("\nEl resultado la suma de %i + %i + %i = %i\n",
           numero1,numero2,numero3,resultado);
    }

    // fin del programa
    return 0;
}
