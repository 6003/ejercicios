// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>
// agrego libreria para obtener el valor de PI
#include <math.h>

// seccion del precopilador

int main()
{
    // Realizar un algoritmo que permita determinar
    // la hipotenusa de un tri�ngulo rect�ngulo.
    // Si se considera como dato las longitudes
    // de sus dos catetos y esto se ingresa por pantalla.

    // Seecion de declaracion de variables locales
    float cateto1;
    float cateto2;
    float resultado;

    // Entrada
    printf("Ingrese el cateto mayor del triangulo rectangulo: ");
    scanf("%f", &cateto1);

    printf("Ingrese el cateto menor del triangulo rectangulo: ");
    scanf("%f", &cateto2);

    // Proceso  I^2 = c^2 + C^2
    resultado = sqrt(pow(cateto1 , 2.0) + pow(cateto2 , 2.0));

    // Salida
    printf("La hipotenusa del triangulo rectangulo es %d\n",resultado);

    // fin del programa
    return 0;
}
