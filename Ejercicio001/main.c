// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Hacer un programa que ingresas un numero entero y por pantalla mostrar el doble de ese numero.

    // Seecion de declaracion de variables locales
    int numero;

    // Entrada
    printf("Ingrese un numero: ");
    scanf("%i",&numero);

    // Salida
    printf("El doble de %i es %i",numero,numero*2);

    // fin del programa
    return 0;
}
