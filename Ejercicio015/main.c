// Seecion de cabecera para incluir las librerias
#include <stdio.h>
#include <stdlib.h>

// seccion del precopilador

int main()
{
    // Hacer un algoritmo que ingresa un n�mero,
    // mostrar por pantalla si es par

    // Seecion de declaracion de variables locales
    int numero;

    // Entrada
    printf("Ingrese un numero: ");
    scanf("%i", &numero);

    // Proceso
    if( numero % 2 == 0){
        // Salida
        printf("\nEl numero %i es par.\n",
           numero);
    }else{
        printf("\nEl numero %i es impar.\n",
           numero);
    }

    // fin del programa
    return 0;
}
